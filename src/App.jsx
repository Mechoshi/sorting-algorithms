import React, { memo, Suspense } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import { Home, NotFound } from '@pages';
import { Loader, Navigation } from '@components';

import styles from './App.scss';

const App = () => {
  return (
    <main className={styles.App}>
      <BrowserRouter>
        <Route path="/" component={Navigation} />
        <Suspense fallback={<Loader />}>
          <Switch className={styles.App}>
            <Route exact path="/" component={Home} />
            <Route path="*" component={NotFound} />
          </Switch>
        </Suspense>
      </BrowserRouter>
    </main>
  );
};

export default memo(App);
