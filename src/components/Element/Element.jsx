import React, { memo } from 'react';

import { classer } from '@services';

import styles from './Element.scss';

const Element = ({ index, isComparing, isSorted, isSwapping, transitionTime = 150, value, width }) => {
  return (
    <li
      className={classer([
        styles.Element,
        isComparing && styles.IsComparing,
        isSorted && !isComparing && !isSwapping && styles.IsSorted,
        isSwapping && styles.IsSwapping
      ])}
      style={{
        width: `calc(${width}% - 1rem)`,
        height: `${value}rem`,
        left: `${index * width}%`,
        transition: `all ${transitionTime}ms ease-in-out`
      }}
    >
      {value}
    </li>
  );
};

export default memo(Element);
