import React, { memo } from 'react';

import styles from './ElementContainer.scss';

const ElementContainer = ({ children }) => {
  return <ul className={styles.ElementContainer}>{children}</ul>;
};

export default memo(ElementContainer);
