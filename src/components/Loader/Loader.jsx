import React, { memo } from 'react';

export const Loader = () => {
  return <div>Loading...</div>;
};

export default memo(Loader);
