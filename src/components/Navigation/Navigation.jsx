import React, { memo } from 'react';
import { NavLink } from 'react-router-dom';

import styles from './Navigation.scss';

export const Navigation = () => {
  return (
    <nav className={styles.Navigation}>
      <NavLink className={styles.NavLink} activeClassName={styles.Active} to="/">
        Home
      </NavLink>
      <NavLink className={styles.NavLink} activeClassName={styles.Active} to="/notfound/page">
        404
      </NavLink>
    </nav>
  );
};

export default memo(Navigation);
