import React, { memo, useCallback, useEffect, useRef, useState } from 'react';

import { bubbleSort, classer, createData, insertionSort, Replayer, quickSort } from '@services';
import { Element, ElementContainer } from '@components';

import styles from './Home.scss';

const SORTING_METHODS = [
  { id: '0', name: 'select method', method: null },
  {
    id: '1',
    info: 'Best: O(n) - when sorted; Average: O(n2); Worst: 0(n2)',
    name: 'Bubble Sort',
    method: bubbleSort
  },
  {
    id: '2',
    info: 'Best: O(n) - when sorted; Average: O(n2); Worst: 0(n2)',
    name: 'Insertion Sort',
    method: insertionSort
  },
  {
    id: '3',
    info: 'Best: O(n.log(n)); Average: O(n.log(n)); Worst: 0(n2) - when sorted',
    name: 'Quick Sort',
    method: quickSort
  }
];

const Home = () => {
  const replayer = useRef(null);
  const [data, setData] = useState(createData(10, false));
  const [isSorting, setIsSorting] = useState(false);
  const [sortingMethod, setSortingMethod] = useState(SORTING_METHODS[0]);
  const [isPaused, setIsPaused] = useState(false);
  const [actionTimeout, setActionTimeout] = useState(600);
  const [operations, setOperations] = useState(0);

  useEffect(() => {
    if (replayer.current) {
      replayer.current.timeout = actionTimeout;
    }
  }, [actionTimeout]);

  const setSpeed = useCallback((direction) => {
    setActionTimeout((currTimeout) => {
      if (direction === '-') {
        return Math.max(100, currTimeout - 100);
      } else {
        return Math.min(2000, currTimeout + 100);
      }
    });
  }, []);

  return (
    <div className={styles.Home}>
      <section>
        <select
          className={styles.Select}
          onChange={(event) => {
            const method = SORTING_METHODS.find((item) => item.id === event.target.value);
            setSortingMethod(method);

            if (replayer.current) {
              replayer.current.reset();
              setOperations(0);
              replayer.current = null;
            }
          }}
          value={sortingMethod.id}
        >
          {SORTING_METHODS.map((method) => {
            return (
              <option key={method.id} name={method.id} value={method.id}>
                {method.name}
              </option>
            );
          })}
        </select>
        <button
          className={classer([styles.ReplayerButton, styles.Pause])}
          disabled={isPaused || !replayer.current}
          onClick={() => {
            if (replayer.current) {
              setIsPaused(true);
              replayer.current.pause();
            }
          }}
          type="button"
        >
          Pause
        </button>
        <button
          className={classer([styles.ReplayerButton, styles.Unpause])}
          disabled={!isPaused}
          onClick={() => {
            if (replayer.current) {
              setIsPaused(false);
              replayer.current.unpause().play();
            }
          }}
          type="button"
        >
          Unpause
        </button>
        <button
          className={styles.ReplayerButton}
          disabled={!replayer.current}
          onClick={() => {
            if (replayer.current) {
              replayer.current.reset();
              setIsPaused(false);
              setOperations(0);
            }
          }}
          type="button"
        >
          Reset
        </button>
        <button
          className={styles.ChangeValueButton}
          disabled={actionTimeout === 100}
          onClick={() => {
            setSpeed('-');
          }}
          type="button"
        >
          -
        </button>
        <span className={styles.ActionTimeout}>{actionTimeout}</span>
        <button
          className={styles.ChangeValueButton}
          disabled={actionTimeout === 2000}
          onClick={() => {
            setSpeed('+');
          }}
          type="button"
        >
          +
        </button>
        <button
          className={styles.Button}
          onClick={() => {
            if (!isSorting) {
              setData(createData(10, true));
            }
          }}
          type="button"
        >
          Set Sorted
        </button>
        <button
          className={styles.Button}
          onClick={() => {
            if (!isSorting) {
              setData(createData(10, false));
            }
          }}
          type="button"
        >
          Set Random
        </button>
        <button
          className={classer([styles.Button, styles.Sort])}
          disabled={sortingMethod.id === '0'}
          onClick={async () => {
            if (!isSorting && sortingMethod && sortingMethod.method) {
              setIsSorting(true);

              replayer.current = new Replayer({
                onReady: (numOfOperations) => {
                  setIsSorting(false);
                  setData(createData(10, false));
                  setOperations(numOfOperations);
                },
                timeout: actionTimeout
              });

              sortingMethod.method({
                array: JSON.parse(JSON.stringify(data)),
                replayer: replayer.current,
                setData
              });
            }
          }}
          type="button"
        >
          Sort
        </button>
      </section>
      <h1 className={styles.Info}>
        {sortingMethod.info}
        <span className={styles.Operations}>{sortingMethod.id !== '0' ? `Operations ${operations}` : null}</span>
      </h1>
      <ElementContainer>
        {data.map((element, index) => (
          <Element
            index={index}
            key={element.id}
            transitionTime={actionTimeout / 3}
            width={100 / data.length}
            {...element}
          />
        ))}
      </ElementContainer>
    </div>
  );
};

export default memo(Home);
