import React, { memo } from 'react';

import styles from './NotFound.scss';

export const NotFound = () => {
  return (
    <div className={styles.NotFound}>
      <h1>Not Found</h1>
    </div>
  );
};

export default memo(NotFound);
