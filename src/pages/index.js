import { lazy } from 'react';

export const Home = lazy(() => import('./Home/Home.jsx'));
export const NotFound = lazy(() => import('./NotFound/NotFound.jsx'));
