import { swap } from './helpers';

export const bubbleSort = ({ array, replayer, setData }) => {
  let minSortedIndex = array.length;

  for (let i = 0; i < array.length; i++) {
    let didSwap = false;
    for (let j = 0; j < minSortedIndex - 1; j++) {
      replayer.add(() => {
        setData((currData) => {
          return currData.map((el, index) => {
            return {
              ...el,
              isComparing: index === j || index === j + 1,
              isSwapping: false
            };
          });
        });
      });

      if (array[j].value > array[j + 1].value) {
        replayer.add(() => {
          setData((currData) => {
            const newData = currData.map((el, index) => {
              return {
                ...el,
                isComparing: false,
                isSwapping: index === j || index === j + 1
              };
            });

            swap(newData, j, j + 1);
            return newData;
          });
        });

        swap(array, j, j + 1);
        didSwap = true;
      }
    }

    if (!didSwap) {
      // If no swapping this round
      // the array is sorted:
      break;
    }

    minSortedIndex--;

    replayer.add(
      // Make sure to get the correct
      // value for `minSortedIndex`:
      (function (minIndex) {
        return () => {
          setData((currData) => {
            return currData.map((el, index) => {
              return {
                ...el,
                isSwapping: false,
                isSorted: minIndex === index ? true : el.isSorted
              };
            });
          });
        };
      })(minSortedIndex)
    );
  }

  replayer
    .add(() => {
      setData((currData) => {
        return currData.map((el) => {
          return {
            ...el,
            isSorted: true
          };
        });
      });
    })
    .ready()
    .play();
};
