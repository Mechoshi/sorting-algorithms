export const classer = (classes = []) => {
  return classes.filter(Boolean).join(' ');
};

const getRandomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min) + min);
};

export const createData = (num, sorted = false) => {
  const data = [];

  for (let i = 0; i < num; i++) {
    data.push({
      id: i,
      isComparing: false,
      isSorted: false,
      isSwapping: false,
      value: sorted ? i + 4 : getRandomInt(4, 50)
    });
  }

  return data;
};

export const swap = (array, i, j) => {
  [array[i], array[j]] = [array[j], array[i]];
};
