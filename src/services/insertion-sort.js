import { swap } from './helpers';

export const insertionSort = ({ array, replayer, setData }) => {
  const { length } = array;

  for (let i = 0; i < length; i++) {
    let start = i;
    while (start > 0) {
      replayer.add(
        // Make sure to get the correct
        // value for `start`:
        (function (currentStart) {
          return () => {
            setData((currData) => {
              return currData.map((el, index) => {
                return {
                  ...el,
                  isComparing: index === currentStart || index === currentStart - 1,
                  isSwapping: false
                };
              });
            });
          };
        })(start)
      );

      if (array[start].value < array[start - 1].value) {
        replayer.add(
          // Make sure to get the correct
          // value for `start`:
          (function (currentStart) {
            return () => {
              setData((currData) => {
                const newData = currData.map((el, index) => {
                  return {
                    ...el,
                    isComparing: false,
                    isSwapping: index === currentStart || index === currentStart - 1
                  };
                });

                swap(newData, currentStart - 1, currentStart);
                return newData;
              });
            };
          })(start)
        );

        swap(array, start - 1, start);
        start--;
      } else {
        break;
      }
    }

    replayer.add(
      // Make sure to get the correct
      // value for `start`:
      (function (currentStart) {
        return () => {
          setData((currData) => {
            return currData.map((el, index) => {
              return {
                ...el,
                isSwapping: false,
                isSorted: currentStart === index ? true : el.isSorted
              };
            });
          });
        };
      })(start)
    );
  }

  replayer
    .add(() => {
      setData((currData) => {
        return currData.map((el) => {
          return {
            ...el,
            isComparing: false,
            isSorted: true,
            isSwapping: false
          };
        });
      });
    })
    .ready()
    .play();
};
