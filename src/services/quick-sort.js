import { swap } from './helpers';

export const quickSort = ({ array, end = array.length - 1, replayer, setData, start = 0 }) => {
  if (end <= start) {
    replayer.add(
      // Make sure to get the correct
      // value for `start`:
      (function (currentStart) {
        return () => {
          setData((currData) => {
            return currData.map((el, index) => {
              return {
                ...el,
                isSwapping: false,
                isSorted: currentStart === index ? true : el.isSorted
              };
            });
          });
        };
      })(start)
    );

    // Start replayer after the `bottom` is reached:
    if (start >= array.length - 1) {
      replayer.ready().play();
    }
    return array;
  }

  let numberOfSmallerValues = 0;

  for (let i = start + 1; i <= end; i++) {
    replayer.add(
      // Make sure to get the correct
      // values for `i` and `start`:
      (function (currentIndex, currentStart) {
        return () => {
          setData((currData) => {
            return currData.map((el, index) => {
              return {
                ...el,
                isComparing: index === currentIndex || index === currentStart,
                isSwapping: false
              };
            });
          });
        };
      })(i, start)
    );
    if (array[i].value < array[start].value) {
      numberOfSmallerValues++;

      if (i > start + numberOfSmallerValues) {
        replayer.add(
          // Make sure to get the correct
          // values for `start`,
          // `numberOfSmallerValues` and `i`:
          (function (currentStart, currentNumberOfSmallerValues, currentIndex) {
            return () => {
              setData((currData) => {
                const newData = currData.map((el, index) => {
                  return {
                    ...el,
                    isComparing: false,
                    isSwapping: index === currentStart + currentNumberOfSmallerValues || index === currentIndex
                  };
                });

                swap(newData, currentStart + currentNumberOfSmallerValues, currentIndex);
                return newData;
              });
            };
          })(start, numberOfSmallerValues, i)
        );

        swap(array, start + numberOfSmallerValues, i);
      }
    }
  }

  replayer.add(
    // Make sure to get the correct
    // values for `start` and
    // `numberOfSmallerValues`:
    (function (currentStart, currentNumberOfSmallerValues) {
      return () => {
        setData((currData) => {
          const newData = currData.map((el, index) => {
            return {
              ...el,
              isComparing: false,
              isSwapping: index === currentStart || index === currentStart + currentNumberOfSmallerValues
            };
          });

          swap(newData, currentStart, currentStart + currentNumberOfSmallerValues);
          return newData;
        });
      };
    })(start, numberOfSmallerValues)
  );

  swap(array, start, start + numberOfSmallerValues);

  replayer.add(
    // Make sure to get the correct
    // values for `start` and
    // `numberOfSmallerValues`:
    (function (currentStart, currentNumberOfSmallerValues) {
      return () => {
        setData((currData) => {
          return currData.map((el, index) => {
            return {
              ...el,
              isSwapping: false,
              isSorted: currentStart + currentNumberOfSmallerValues === index ? true : el.isSorted
            };
          });
        });
      };
    })(start, numberOfSmallerValues)
  );

  quickSort({ array, end: start + numberOfSmallerValues - 1, replayer, setData, start });
  quickSort({ array, end, replayer, setData, start: start + numberOfSmallerValues + 1 });

  return array;
};
