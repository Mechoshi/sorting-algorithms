/* eslint-disable no-lonely-if */
/* eslint-disable no-await-in-loop */
/* eslint-disable generator-star-spacing */
export class Replayer {
  constructor({ onReady, timeout }) {
    this.frames = [];
    this.isPaused = false;
    this.isReadyToPlay = false;
    this.onReady = onReady || null;
    this.operations = 0;
    this.player = null;
    this.timeout = timeout;
  }

  ready() {
    this.player = this.playerGenerator();
    this.isReadyToPlay = true;

    return this;
  }

  async play() {
    if (this.player && !this.isPaused) {
      const data = await this.player.next();

      if (!data.done) {
        this.play();
      } else {
        if (typeof this.onReady === 'function') {
          this.onReady(this.operations);
        }
      }
    }
  }

  pause() {
    this.isPaused = true;
    return this;
  }

  unpause() {
    this.isPaused = false;
    return this;
  }

  reset() {
    this.frames = [];
    this.isPaused = false;
    this.isReadyToPlay = false;
    this.operations = 0;
    this.player = null;

    if (typeof this.onReady === 'function') {
      this.onReady();
    }
    return this;
  }

  add(action) {
    if (!this.isReadyToPlay) {
      this.frames.push(action);
    }
    return this;
  }

  async *playerGenerator() {
    for (let i = 0; i <= this.frames.length; i++) {
      yield new Promise((resolve) => {
        if (typeof this.frames[i] === 'function') {
          this.frames[i]();
          this.operations++;
        }
        setTimeout(() => resolve(i), this.timeout);
      });
    }
  }
}
