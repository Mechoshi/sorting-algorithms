import { applyMiddleware, compose, createStore } from "redux";
import thunk from "redux-thunk";

import rootReducer from "./reducers";
import { ajax } from "../services";

const composeEnhancers =
  process.env.NODE_ENV === "production" ? compose : window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk.withExtraArgument(ajax))));

ajax.__dispatch = store.dispatch;

export default store;
