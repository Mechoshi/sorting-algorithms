import { combineReducers } from 'redux';

import * as actions from '../action-types';

const app = (state = {}, { payload, type }) => {
  switch (type) {
    default:
      return state;
  }
};

export default combineReducers({
  app
});
